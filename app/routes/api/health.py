import os
import json
from flask import request, app, Blueprint, jsonify
from flask_restful import Resource, Api

health_bp = Blueprint('health', __name__)
health_api = Api(health_bp, prefix='/api/v1/health')


class Health(Resource):

    def get(self):
        """
        Health result
        """

        return jsonify({
            'status': 'OK'
        })

health_api.add_resource(Health, '/')
